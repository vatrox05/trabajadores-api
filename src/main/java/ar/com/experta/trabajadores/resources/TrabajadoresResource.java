package ar.com.experta.trabajadores.resources;


import ar.com.experta.trabajadores.core.TrabajadoresService;
import ar.com.experta.trabajadores.entities.TrabajadorFilter;
import ar.com.experta.trabajadores.entities.TrabajadorOut;
import ar.com.experta.trabajadores.providers.ErrorResponse;
import ar.com.experta.trabajadores.utils.TrabajadoresUtils;
import com.codahale.metrics.health.HealthCheck;
import com.codahale.metrics.health.HealthCheckRegistry;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Map;
import java.util.Set;

@Path("/trabajadores")
@Api(value="/trabajadores", description="Operaciones para la Api de Trabajadores")
public class TrabajadoresResource {

    private  HealthCheckRegistry registry;
    private Logger log = Logger.getLogger(this.getClass().getName());
    private TrabajadoresUtils trabajadoresUtils = TrabajadoresUtils.getInstance();
    public TrabajadoresResource() throws  Exception{

    }

    TrabajadoresService trabajadoresService = TrabajadoresService.getInstance();

/*------------------------------------------------------------------------------------ */
    @GET
    @Path("/{cuil}")
    @ApiOperation(
            value = "getTrabajadorPorCuit",
            response = TrabajadorOut.class)
    public Response getTrabajadorPorCuit(@PathParam(value = "cuil") String cuil) throws Exception{

        Response response = Response.noContent().build();

        if (StringUtils.isNotEmpty(cuil)){
            try {
                TrabajadorFilter trabajadorFilter = new TrabajadorFilter(cuil);
                TrabajadorOut trabajadorOut = trabajadoresService.getTrabajadorPorCuit(trabajadorFilter);
                if (trabajadorOut == null){
                    return response = Response.status(Response.Status.NO_CONTENT).build();
                }
                response = Response.ok(trabajadorOut).build();

            }catch (Exception e){
                log.error("Error en la consulta de TrabajadorPorCuit",e);
                response = Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(
                                                    new ErrorResponse(
                                                                String.valueOf( Response.Status.SERVICE_UNAVAILABLE.getStatusCode()),
                                                                "Error en la consulta de TrabajadorPorCuit",
                                                                trabajadoresUtils.getStackTrace(e))
                ).build();

            }
        }else {
            log.error("No se envió campo cuil");
            response = Response.status(Response.Status.BAD_REQUEST).entity(
                    new ErrorResponse(
                            String.valueOf( Response.Status.BAD_REQUEST.getStatusCode()),
                            "No se envió campo cuil")
            ).build();
        }

        return response;
    }
/*------------------------------------------------------------------------------------ */


/*------------------------------------------------------------------------------------ */
    @GET
    @ApiOperation(
            value = "getTrabajadorFilter",
            response = TrabajadorOut.class)
    public Response getTrabajadorFilter (@QueryParam(value = "dni") String dni,
                                         @QueryParam(value = "nombre") String nombre) throws Exception{

        Response response = Response.noContent().build();

        if (StringUtils.isNotEmpty(dni) || StringUtils.isNotEmpty(nombre)){
            try {
                TrabajadorFilter trabajadorFilter = new TrabajadorFilter(dni,nombre);
                TrabajadorOut trabajadorOut = trabajadoresService.getTrabajadorFilter(trabajadorFilter);
                if (trabajadorOut == null){
                    return response = Response.status(Response.Status.NO_CONTENT).build();
                }
                response = Response.ok(trabajadorOut).build();

            }catch (Exception e){

                log.error("Error en la consulta de TrabajadorFilter",e);
                response = Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(
                        new ErrorResponse(
                                String.valueOf( Response.Status.SERVICE_UNAVAILABLE.getStatusCode()),
                                "Error en la consulta de TrabajadorFilter",
                                trabajadoresUtils.getStackTrace(e))
                ).build();

            }
        }else {
            log.error("No se envió campos para filtrar la consulta: dni y/o nombre ");
            response = Response.status(Response.Status.BAD_REQUEST).entity(
                    new ErrorResponse(
                            String.valueOf( Response.Status.BAD_REQUEST.getStatusCode()),
                            "No se envió campos para filtrar la consulta: dni y/o nombre ")
            ).build();
        }

        return response;
    }
/*------------------------------------------------------------------------------------ */
}
