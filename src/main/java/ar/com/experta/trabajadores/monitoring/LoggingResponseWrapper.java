package ar.com.experta.trabajadores.monitoring;

import org.apache.log4j.Logger;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;

public class LoggingResponseWrapper  extends HttpServletResponseWrapper {

    private static final Logger logger = Logger.getLogger(LoggingResponseWrapper.class);

    TeeServletOutputStream teeStream;

    PrintWriter teeWriter;

    ByteArrayOutputStream bos;

    public LoggingResponseWrapper(HttpServletResponse response) {
        super(response);
    }

    public String getContent() throws IOException {
        if (bos != null){
            String payload = bos.toString();
            if (payload.length()>30500){
                payload = payload.substring(0,30000);
                return payload;
            }else {
                return payload;
            }
        }else {
            return null;
        }

    }

    @Override
    public PrintWriter getWriter() throws IOException {

        if (this.teeWriter == null) {
            this.teeWriter = new PrintWriter(new OutputStreamWriter(getOutputStream()));
        }
        return this.teeWriter;
    }

    @Override
    public ServletOutputStream getOutputStream() throws IOException {

        if (teeStream == null) {
            bos = new ByteArrayOutputStream();
            teeStream = new TeeServletOutputStream(getResponse().getOutputStream(), bos);
        }
        return teeStream;
    }

    @Override
    public void flushBuffer() throws IOException {
        if (teeStream != null) {
            teeStream.flush();
            System.err.println("teeStream flush");
        }
        if (this.teeWriter != null) {
            this.teeWriter.flush();
            System.err.println("teeWriter flush");
        }
    }
}

