package ar.com.experta.trabajadores.monitoring;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

public class MonitoringModel {

    private static MonitoringModel monitoringModel =new MonitoringModel();

    public static MonitoringModel getInstance(){
        if (monitoringModel != null){
            return monitoringModel;
        }
        return new MonitoringModel();
    }

    private String operation ;
    private String applicationName;
    private int maxEntitySize = 30000;
    private String httpRequestLog = null;
    private String httpResponseLog = null;
    private String currentMethod;
    private Long excutionTime;
    private Integer httpResponseCode;
    private String usuario = "Anonymous";
    private boolean notCatchedException = false;
    private int maxResponseLength = 30000;


    Logger log = Logger.getLogger(MonitoringModel.class);

    public  String getOperation() {

        if(StringUtils.isEmpty(this.operation)){
            this.operation="Unknown";
        }
        return this.operation;
    }

    public  void setOperation(String operation) {
        this.operation = operation;
    }

    public String getApplicationName() {
        return this.applicationName;
    }

    public void setApplicactionName(String applicationName) {
        this.applicationName = applicationName;
    }

    public int getMaxEntitySize() {
        return this.maxEntitySize;

    }

    public void setMaxEntitySize(int maxEntitySize) {
        this.maxEntitySize = maxEntitySize;
    }

    public void setHttpRequestLog(String httpRequestLog) {
        this.httpRequestLog = "\n [----"+httpRequestLog+"----]";
    }

    public String getHttpRequestLog() {
        return httpRequestLog;
    }

    public void setHttpResponseLog(String httpResponseLog) {

        if (httpResponseLog.length()> maxResponseLength){
            this.httpResponseLog = "\n [----" +httpResponseLog.substring(0,maxResponseLength)+"----]";
        }else {
            this.httpResponseLog = "\n [----" +httpResponseLog+"----]";
        }

    }

    public String getHttpResponseLog() {
        return httpResponseLog;
    }


    public void setCurrentMethod(String currentMethod) {
        this.currentMethod = currentMethod;
    }

    public String getCurrentMethod() {

        if(StringUtils.isEmpty(this.currentMethod)){
            this.currentMethod="Unknown";
        }
        return this.currentMethod;

    }

    public void setExcutionTime(Long excutionTime) {
        this.excutionTime = excutionTime;
    }

    public Long getExcutionTime() {
        if (this.excutionTime==null){
            excutionTime= Long.valueOf(0);
        }
        return excutionTime;
    }

    public void setHttpResponseCode(int httpResponseCode) {
        this.httpResponseCode = httpResponseCode;
    }

    public Integer getHttpResponseCode() {
        return httpResponseCode;
    }

    public String getResultStatus(){
        String statusRespuesta = "Unknown";

        if (httpResponseCode==null){
            return statusRespuesta;
        }else {
            try {
                String code = httpResponseCode.toString().substring(0,1);
                if (code.equals("2")){
                    statusRespuesta= "OK";
                }else{
                    statusRespuesta= "ERROR";
                }
            }catch (Exception e){
                log.error(" Error when getting the Http Status ",e);

            }
        }
        return statusRespuesta;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setNotCatchedException(boolean notCatchedException) {
        this.notCatchedException = notCatchedException;
    }

    public boolean isNotCatchedException() {
        return notCatchedException;
    }

    public String getObjectToJson(Object pointObj) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();

        // Convert object to JSON string and pretty print
        String jsonInString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(pointObj);

        return jsonInString;
    }

    public void initLogState() {
        setNotCatchedException(false);
        setHttpResponseLog("");
        setHttpRequestLog("");
        setCurrentMethod(null);
        setExcutionTime(null);
    }
}
