package ar.com.experta.trabajadores.monitoring;


import org.apache.commons.lang3.time.StopWatch;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Enumeration;

public class MonitoringFilter implements Filter {
    MonitoringModel monitoringModel = MonitoringModel.getInstance();
    private StopWatch stopWatch = new StopWatch();

    public MonitoringFilter() {
    }
    Logger logger = Logger.getLogger("Monitoring");
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
        LoggingRequestWrapper loggingRequestWrapper = null;
        stopWatch.reset();
        stopWatch.start();
        try {
            loggingRequestWrapper = new LoggingRequestWrapper((HttpServletRequest) request);
        } catch (Exception e) {
            e.printStackTrace();
        }
        LoggingResponseWrapper loggingResponseWrapper = new LoggingResponseWrapper((HttpServletResponse) response);

        String headersString = "";
        Enumeration<String> e = loggingRequestWrapper.getHeaderNames();
        while (e.hasMoreElements()) {
            String headers = e.nextElement();
            if (headers != null) {
                headersString += headers + " :: " + loggingRequestWrapper.getHeader(headers) + " , ";
            }
        }

        monitoringModel.setHttpRequestLog("REST Request Detail-----" + " \n RequestURI :: "
                + loggingRequestWrapper.getRequestURI() + " \n REMOTE ADDRESS :: " + loggingRequestWrapper.getRemoteAddr()
                + " \n HEADERS :: [ " + headersString + " ] " + " \n REQUEST BODY Size :: " + loggingRequestWrapper.payload.length()
                + " bytes" + " \n REQUEST BODY :: " + loggingRequestWrapper.payload + " \n HTTP METHOD :: "
                + loggingRequestWrapper.getMethod() + " \n ContentType :: " + loggingRequestWrapper.getContentType());


        filterChain.doFilter(loggingRequestWrapper, loggingResponseWrapper);

        monitoringModel.setHttpResponseCode(loggingResponseWrapper.getStatus());

        if (!monitoringModel.isNotCatchedException()){
            monitoringModel.setHttpResponseLog("REST Response Detail----------"
                    + " \n ContentType :: " + loggingRequestWrapper.getContentType()
                    +" \n Response BODY Size :: "
                    + (loggingResponseWrapper.getContent()!= null ? loggingResponseWrapper.getContent().length(): 0 )
                    + " bytes"
                    +" \n Http Status :: "+loggingResponseWrapper.getStatus()
                    + " \n Response BODY :: "
                    +  (loggingResponseWrapper.getContent()!= null ? loggingResponseWrapper.getContent() : ""));
        }

        stopWatch.stop();

        monitoringModel.setExcutionTime(stopWatch.getTime());

        String logMessageMetrics = String.format("%1$s %2$s %3$s %4$s %5$s ",
                monitoringModel.getApplicationName(),
                monitoringModel.getCurrentMethod(),
                monitoringModel.getUsuario(),
                monitoringModel.getExcutionTime(),
                monitoringModel.getResultStatus()
                );

        logger.info(logMessageMetrics);
        logger.debug(String.format("%1$s %2$s",
                monitoringModel.getHttpRequestLog(),
                monitoringModel.getHttpResponseLog()));

        monitoringModel.initLogState();

    }

    @Override
    public void destroy() {
    }


}
