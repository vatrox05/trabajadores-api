package ar.com.experta.trabajadores.monitoring;


import com.fasterxml.jackson.core.JsonProcessingException;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;


@Provider
@Aspect
public class MonitoringAspects {
    private MonitoringModel monitoringModel = MonitoringModel.getInstance();

    public MonitoringAspects() {
    }
//    ar.com.experta.trabajadores.resources
    @Around("execution(* ar.com.experta.trabajadores.resources.TrabajadoresResource.*(..))")
    public Object around(ProceedingJoinPoint point) throws Exception {
        long start = System.currentTimeMillis();

        //se guarda el recurso ejecutado
        monitoringModel.setCurrentMethod(Thread.currentThread().getStackTrace()[2].getMethodName());

        Object pointObj = point.getArgs()[0];

        Object result = null;

        try {
            result = point.proceed();

        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }

        return result;
    }

    @Before("execution(* ar.com.experta.trabajadores.providers.ErrorHandler*.handleException(..))")
    public void before(JoinPoint joinPoint) throws Exception {
        Response response = null;

        for (Object object: joinPoint.getArgs()){
            if (object instanceof Response){
                 response = (Response) object;
            }
        }

        monitoringModel.setNotCatchedException(true);

        String json;

        try {
            json = monitoringModel.getObjectToJson(response.getEntity());
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            json = "No se logro parsear la respuesta ";
        }

        monitoringModel.setHttpResponseLog("\n REST Response Detail----------"
                + " \n ContentType :: " + response.getMediaType()
                +" \n Http Status :: "+response.getStatus()
                + " \n Response BODY :: "
                +  (response.getEntity()!= null ?   json : ""));

    }



}





