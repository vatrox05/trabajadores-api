package ar.com.experta.trabajadores.utils;

import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

public class TraduccionesUtils {

    HashMap<String, String> tipoTraslados = new HashMap<>();
    HashMap<String, String> diagnosticos = new HashMap<>();
    HashMap<String, String> descEstado = new HashMap<>();
    HashMap<String, String> estados = new HashMap<>();
    HashMap<String, String> coberturas = new HashMap<>();

    private static final TraduccionesUtils traduccionesUtils =new TraduccionesUtils();

    private TraduccionesUtils(){
        initDictionary();
    }
    public static TraduccionesUtils getInstance(){
        if (traduccionesUtils != null){
            return traduccionesUtils;
        }
        return new TraduccionesUtils();
    }

    public void initDictionary() {

         /*Adding elements to HashMap*/
        diagnosticos.put(" HC ", " Historia Clínica ");
        diagnosticos.put(" FX ", " Fractura ");
        diagnosticos.put(" CX ", " Cirugía ");
        diagnosticos.put(" QX ", " Cirugía ");
        diagnosticos.put(" LII ", " Columna o lumbalgia ");
        diagnosticos.put(" LI ", " Columna o lumbalgia ");
        diagnosticos.put(" TXII-LI ", " Columna o lumbalgia ");
        diagnosticos.put(" TXII-LII ", " Columna o lumbalgia ");
        diagnosticos.put(" L1 ", " Columna o lumbalgia ");
        diagnosticos.put(" L2 ", " Columna o lumbalgia ");
        diagnosticos.put(" AINE ", " Anti- inflamatorio no esteroide ");
        diagnosticos.put(" TKF ", " Kinesiología ");
        diagnosticos.put(" FKT ", " Kinesiología ");
        diagnosticos.put(" KTR ", " Kinesiología ");
        diagnosticos.put(" KT ", " Kinesiología ");
        diagnosticos.put(" AT ", " antibiótico ");
        diagnosticos.put(" ATB ", " antibiótico ");
        diagnosticos.put(" ATT ", " Antitetánica ");
        diagnosticos.put(" RMN ", " Resonancia Magnética ");
        diagnosticos.put(" XC1 ", " código de prestador  ");
        diagnosticos.put(" TEC ", " Tomografía endocraneana ");
        diagnosticos.put(" TTO ", " tratamiento ");
        diagnosticos.put(" TMO ", " traumatismo ");
        diagnosticos.put(" TX ", " traumatismo ");
        diagnosticos.put(" RX ", " Rayos X  ");
        diagnosticos.put(" TAC ", " Tomografía computada  ");
        diagnosticos.put(" EMG ", " Electromiograma ");
        diagnosticos.put(" IC ", " Inter consulta ");
        diagnosticos.put(" ORL ", " Otorrinolaringólogo ");
        diagnosticos.put(" ICORL ", " inter consulta con otorrinolaringólogo ");

        tipoTraslados.put("SIN TRASLADOS", "Traslado por sus medios");
        tipoTraslados.put("NORMAL", "Coordinado por Experta ART");
        tipoTraslados.put("AUTOGESTION", "Anticipo");
        tipoTraslados.put("SE TRASLADA POR SUS MEDIOS", "Traslado por sus medios");

        descEstado.put("inmediata","Alta Inmediata");
        
        estados.put(" C ", "Cerrado");
        estados.put(" A ", "Abierto");
        estados.put(" R ", "Reabierto");
        estados.put(" E ", "Cancelado");
        
        coberturas.put(" A ", "Con Cobertura");
        coberturas.put(" B ", "No - Acc. fuera de Nominas");
        coberturas.put(" C ", "No - Stro. fuera de Vigencia");
        coberturas.put(" D ", "No - Ctro. cancelado por O. de");
    }
    
    public String getDiagnosticoTraducido(String diagnosticoFromBase){
        if(StringUtils.isEmpty(diagnosticoFromBase)){
            return null;
        }
        return replaceKeysFromText(this.diagnosticos, diagnosticoFromBase);
    }

    public String getTipoTrasladoTraducido(String tipoTrasladoFromBase){
        if(StringUtils.isEmpty(tipoTrasladoFromBase)){
            return null;
        }
        return replaceKeysFromText(this.tipoTraslados,tipoTrasladoFromBase);
    }
    
    public String getDescEstadoTraducido(String descEstadoFromBase){
        if(StringUtils.isEmpty(descEstadoFromBase)){
            return null;
        }
        return replaceKeysFromText(this.descEstado, descEstadoFromBase);
    }
    /**
     * Este método se utiliza para traducir el 'estado' del Siniestro obtenido de la Base de Datos.
     * @param estadoFromBase Estado devuelto desde la Base de Datos.
     * @return Retorna la traducción del 'estado'.
     */
    public String getEstadoTraducido(String estadoFromBase){
        if(StringUtils.isEmpty(estadoFromBase)){
            return null;
        }
        return replaceKeysFromText(this.estados, estadoFromBase);
    }
    /**
     * Este método se utiliza para traducir la 'cobertura' del Siniestro obtenida de la Base de Datos.
     * @param coberturaFromBase Cobertura devuelta desde la Base de Datos.
     * @return Retorna la traducción de la 'cobertura'.
     */
    public String getCoberturaTraducida(String coberturaFromBase){
        if(StringUtils.isEmpty(coberturaFromBase)){
            return null;
        }
        return replaceKeysFromText(this.coberturas, coberturaFromBase);
    }
    /**
     * Este método es el encargado de traducir los campos enviados.
     * @param hmap Map de traducción.
     * @param targetText String a traducir.
     * @return 
     */
    public String replaceKeysFromText(HashMap<String,String> hmap,String targetText){
        Set set = hmap.entrySet();
        Iterator iterator = set.iterator();

        targetText=" "+targetText+" ";

        while(iterator.hasNext()) {
            Map.Entry mentry = (Map.Entry)iterator.next();

            targetText = targetText.replaceAll( "(?i)"+ Pattern.quote((String) mentry.getKey()),
                                               (String) hmap.get((String) mentry.getKey()));
        }

        return targetText;
    }
}
