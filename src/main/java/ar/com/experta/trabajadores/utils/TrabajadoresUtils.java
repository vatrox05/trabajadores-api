package ar.com.experta.trabajadores.utils;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TrabajadoresUtils {
    private static TrabajadoresUtils trabajadoresUtils = new TrabajadoresUtils();

    public static TrabajadoresUtils getInstance() {
        return trabajadoresUtils;
    }

    public List<String> getStackTrace(Exception exception) {
        List<String> stackList = new ArrayList<>();

        final StringWriter sw = new StringWriter();
        final PrintWriter pw = new PrintWriter(sw, true);
        exception.printStackTrace(pw);
        String[] stackArray = sw.getBuffer().toString().split("\\n");
        stackList = Arrays.asList(stackArray);

        return stackList;
    }
}
