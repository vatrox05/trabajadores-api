package ar.com.experta.trabajadores.core;

import ar.com.experta.trabajadores.db.TrabajadoresDao;
import ar.com.experta.trabajadores.entities.TrabajadorFilter;
import ar.com.experta.trabajadores.entities.TrabajadorOut;

public class TrabajadoresService {

    private static TrabajadoresService trabajadoresService ;
    private static TrabajadoresDao trabajadoresDao = null;


    public static TrabajadoresService getInstance()  {

        if (trabajadoresService == null ){
            trabajadoresService = new TrabajadoresService(trabajadoresDao);
        }

        return trabajadoresService;
    }

    public TrabajadoresService(TrabajadoresDao trabajadoresDao)  {
        this.trabajadoresDao = trabajadoresDao;
    }

    public TrabajadorOut getTrabajadorPorCuit(TrabajadorFilter trabajadorFilter) throws Exception {
        return trabajadoresDao.getTrabajador(trabajadorFilter);
    }


    public TrabajadorOut getTrabajadorFilter(TrabajadorFilter trabajadorFilter)  throws Exception {
        return trabajadoresDao.getTrabajador(trabajadorFilter);
    }

}
