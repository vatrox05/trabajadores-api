package ar.com.experta.trabajadores.db.mapper;

import ar.com.experta.trabajadores.entities.DireccionOut;
import ar.com.experta.trabajadores.entities.TrabajadorOut;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TrabajadorMapper implements ResultSetMapper<TrabajadorOut> {
    @Override
    public TrabajadorOut map(int i, ResultSet r, StatementContext statementContext) throws SQLException {
        return new TrabajadorOut(
                r.getString("CUIL"),
                r.getString("DNI"),
                r.getString("CEDULAIDENTIDAD"),
                r.getString("DOCUMENTOUNICO"),
                r.getString("NOMBRE"),
                new DireccionOut(r.getString("CALLE"),
                                r.getString("NUMERO"),
                                r.getString("PISO"),
                                r.getString("DEPARTAMENTO"),
                                r.getString("CODIGO_POSTAL"),
                                r.getString("LOCALIDAD"),
                                r.getString("PROVINCIA")),
                r.getString("TIPO_DOCUMENTO")

        );
    }
}
