package ar.com.experta.trabajadores.db;

import ar.com.experta.trabajadores.db.mapper.TrabajadorMapper;
import ar.com.experta.trabajadores.entities.TrabajadorFilter;
import ar.com.experta.trabajadores.entities.TrabajadorOut;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.customizers.Mapper;

public interface TrabajadoresDao {

    //select para consultar los trabajadores por dni y/o nombre
    @SqlQuery("SELECT \n" +
            "  substr(p.dni,1,1) tipo_documento,\n" +
            "  substr(p.dni,2) cuil,\n" +
            "  substr(p.dni,2) dni,\n" +
            "  substr(p.dni,2) cedulaIdentidad,\n" +
            "  substr(p.dni,2) documentoUnico,\n" +
            "  p.nombre nombre,\n" +
            "  p.domicilio calle,\n" +
            "  p.nmcasa numero,\n" +
            "  p.nmpiso piso,\n" +
            "  p.nmpuerta departamento,\n" +
            "  p.cdpostal codigo_postal,\n" +
            "  postal.poblacion localidad,\n" +
            "  provincias.provin provincia\n" +
            "  FROM PERSONAS p \n" +
            "    join art.PROVINCIAS provincias\n" +
            "      on p.cod_prov=provincias.codprov\n" +
            "    join art.POSTAL postal\n" +
            "      on p.cdpostal=postal.cdpostal\n" +
            "  WHERE  (substr(p.dni,2)=:in.dni  OR :in.dni  IS NULL) AND\n" +
            "         (LOWER(p.nombre) LIKE '%' || LOWER(:in.nombre) || '%' OR :in.nombre  IS NULL)")
    @Mapper(TrabajadorMapper.class)
    TrabajadorOut getTrabajador(@BindBean("in") TrabajadorFilter trabajadorFilter) throws Exception;

}
