package ar.com.experta.trabajadores.providers;

import java.util.List;

public class ErrorResponse  {
    private String error_code ;
    private String error_message;
    private List<String> errorRuntime;

    public ErrorResponse() {
    }

    public ErrorResponse(String error_code, String error_message) {
        this.error_code = error_code;
        this.error_message = error_message;
    }

    public ErrorResponse(String errorCode, String errorMessage, List<String> errorRuntime) {
        this.error_code = errorCode;
        this.error_message = errorMessage;
        this.errorRuntime = errorRuntime;
    }

    public ErrorResponse(String codigoRespuesta) {
        this.error_code = codigoRespuesta;
    }
    public List<String> getErrorRuntime() {
        return errorRuntime;
    }

    public void setErrorRuntime(List<String> errorRuntime) {
        this.errorRuntime = errorRuntime;
    }

	public String getError_code() {
		return error_code;
	}

	public void setError_code(String error_code) {
		this.error_code = error_code;
	}

	public String getError_message() {
		return error_message;
	}

	public void setError_message(String error_message) {
		this.error_message = error_message;
	}
    
}
