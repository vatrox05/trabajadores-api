package ar.com.experta.trabajadores.providers;

import ar.com.experta.trabajadores.monitoring.MonitoringModel;
import ar.com.experta.trabajadores.utils.TrabajadoresUtils;
import org.apache.log4j.Logger;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;


/**
 * Created by Vatrox10 on 9/28/2017.
 */
public class ErrorHandler implements ExceptionMapper<Exception> {
    private TrabajadoresUtils trabajadoresUtils = TrabajadoresUtils.getInstance();
    private MonitoringModel monitoringModel = MonitoringModel.getInstance();
    Logger log = Logger.getLogger(ErrorHandler.class);
    private Response.Status status = Response.Status.NOT_FOUND;

    @Override
    public Response toResponse(Exception exception) {


        Response response = Response
                .status(this.status)
                .entity(new ErrorResponse(String.valueOf(this.status),
                        "Error",trabajadoresUtils.getStackTrace(exception)))
                .build();

        log.error("Error",exception);

        if (exception instanceof Exception) {
            return handleException(response);
        }

        log.debug("Uncaught exception in application", exception);

        return response;
    }

    private Response handleException( Response response) {


        return response;
    }


}
