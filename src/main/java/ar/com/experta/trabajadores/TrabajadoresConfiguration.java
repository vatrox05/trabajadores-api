package ar.com.experta.trabajadores;

import com.serviceenabled.dropwizardrequesttracker.RequestTrackerConfiguration;
import io.dropwizard.Configuration;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.db.DataSourceFactory;
import org.hibernate.validator.constraints.*;

import javax.validation.Valid;
import javax.validation.constraints.*;

public class TrabajadoresConfiguration extends Configuration {

    @NotNull
    @Valid
    private DataSourceFactory dataSourceFactoryTrabajadores = new DataSourceFactory();
    @JsonProperty("trabajadoresDS")
    public DataSourceFactory getDataSourceFactoryTrabajadores() {
        return dataSourceFactoryTrabajadores;
    }


    private RequestTrackerConfiguration requestTrackerConfiguration = new RequestTrackerConfiguration();

    public RequestTrackerConfiguration getRequestTrackerConfiguration() {
        return requestTrackerConfiguration;
    }

    public void setRequestTrackerConfiguration(RequestTrackerConfiguration configuration) {
        this.requestTrackerConfiguration = configuration;
    }
}
