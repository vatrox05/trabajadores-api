package ar.com.experta.trabajadores;

import ar.com.experta.trabajadores.core.TrabajadoresService;
import ar.com.experta.trabajadores.db.TrabajadoresDao;
import ar.com.experta.trabajadores.health.AppHealthCheck;
import ar.com.experta.trabajadores.health.DatabaseHealthCheck;
import ar.com.experta.trabajadores.monitoring.MonitoringAspects;
import ar.com.experta.trabajadores.monitoring.MonitoringFilter;
import ar.com.experta.trabajadores.monitoring.MonitoringModel;
import ar.com.experta.trabajadores.providers.ErrorHandler;
import ar.com.experta.trabajadores.resources.TrabajadoresResource;
import ar.com.experta.trabajadores.utils.TraduccionesUtils;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.serviceenabled.dropwizardrequesttracker.RequestTrackerBundle;
import com.serviceenabled.dropwizardrequesttracker.RequestTrackerConfiguration;
import io.dropwizard.Application;
import io.dropwizard.jdbi.DBIFactory;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.swagger.jaxrs.config.BeanConfig;
import io.swagger.jaxrs.listing.ApiListingResource;
import org.joda.time.DateTimeZone;
import org.skife.jdbi.v2.DBI;

import javax.servlet.DispatcherType;
import java.util.EnumSet;

public class TrabajadoresApplication extends Application<TrabajadoresConfiguration> {

    public static void main(final String[] args) throws Exception {
        new TrabajadoresApplication().run(args);
    }

    @Override
    public String getName() {
        return "trabajadores-api";
    }

    @Override
    public void initialize(final Bootstrap<TrabajadoresConfiguration> bootstrap) {
        DateTimeZone.setDefault(DateTimeZone.UTC);
        bootstrap.addBundle(new RequestTrackerBundle<TrabajadoresConfiguration>() {
            @Override
            public RequestTrackerConfiguration getRequestTrackerConfiguration(TrabajadoresConfiguration trabajadoresConfiguration) {
                return trabajadoresConfiguration.getRequestTrackerConfiguration();
            }
        });
    }

    @Override
    public void run(final TrabajadoresConfiguration configuration,
                    final Environment environment) throws Exception{


        final DBIFactory factoryTrabajadores = new DBIFactory();
        final DBI jdbiTrabajadores = factoryTrabajadores.build(environment,configuration.getDataSourceFactoryTrabajadores(),"trabajadoresDS");
        final TrabajadoresDao trabajadoresDao = jdbiTrabajadores.onDemand(TrabajadoresDao.class);

//      Guardar el nombre de la applicación para ser impreso en los logs de monitoreo
        MonitoringModel monitoringModel = MonitoringModel.getInstance();
        monitoringModel.setApplicactionName(getName());

        environment.healthChecks().register("databaseTrabajadores", new DatabaseHealthCheck(configuration.getDataSourceFactoryTrabajadores()));
        environment.healthChecks().register("application", new AppHealthCheck());

        TrabajadoresResource trabajadoresResource = new TrabajadoresResource();

        TrabajadoresService trabajadoresService = new TrabajadoresService(trabajadoresDao);
        ErrorHandler errorHandler =new ErrorHandler();
        TraduccionesUtils traduccionesUtils = TraduccionesUtils.getInstance();
        MonitoringAspects monitoringAspects = new MonitoringAspects();

//      iniciar el filtro servlet para la captura de los tiempos de ejecución y los payloads.
        environment.servlets()
                .addFilter("api-name-servlet-filter", new MonitoringFilter())
                .addMappingForUrlPatterns(EnumSet.of(DispatcherType.REQUEST), false, "*");

        environment.jersey().register(monitoringAspects);
        environment.jersey().register(traduccionesUtils);

        environment.jersey().register(trabajadoresResource);
        environment.jersey().register(trabajadoresService);
        environment.jersey().register(errorHandler);


        environment.getObjectMapper().setSerializationInclusion(JsonInclude.Include.NON_NULL);
        environment.jersey().register(new ApiListingResource());
        BeanConfig config = new BeanConfig();
        config.setTitle("Trabajadores API");
        config.setVersion("1.0.0");
        config.setResourcePackage("ar.com.experta.trabajadores.resources");
        config.setScan(true);
    }

}
