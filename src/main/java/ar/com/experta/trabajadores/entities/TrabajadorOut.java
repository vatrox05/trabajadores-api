package ar.com.experta.trabajadores.entities;

public class TrabajadorOut {

    String cuil;
    String dni;
    String documentoUnico;
    String cedulaIdentidad;
    String nombre;
    DireccionOut direccion;
    String tipoDocumento;

    public TrabajadorOut(String cuil, String dni, String documentoUnico,
                         String cedulaIdentidad, String nombre, DireccionOut direccion, String tipoDocumento) {





        this.cuil = cuil;
        this.dni = dni;
        this.documentoUnico = documentoUnico;
        this.cedulaIdentidad = cedulaIdentidad;
        this.nombre = nombre;
        this.direccion = direccion;
    }

    public String getCuil() {
        return cuil;
    }

    public void setCuil(String cuil) {
        this.cuil = cuil;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getDocumentoUnico() {
        return documentoUnico;
    }

    public void setDocumentoUnico(String documentoUnico) {
        this.documentoUnico = documentoUnico;
    }

    public String getCedulaIdentidad() {
        return cedulaIdentidad;
    }

    public void setCedulaIdentidad(String cedulaIdentidad) {
        this.cedulaIdentidad = cedulaIdentidad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public DireccionOut getDireccion() {
        return direccion;
    }

    public void setDireccion(DireccionOut direccion) {
        this.direccion = direccion;
    }
}




